import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/authentication/roots/login-register.dart';
import 'package:teori_prove/categories/categories_page.dart';
import 'package:teori_prove/common/dialog.dart';
import 'package:teori_prove/exam/category_detail_bloc_provider.dart';
import 'package:teori_prove/exam/exam-page.dart';
import 'package:teori_prove/home/home_bloc.dart';
import 'package:teori_prove/models/app-tariff-type.dart';
import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/models/user-repository.dart';
import 'package:teori_prove/pages/category-item.dart';
import 'package:teori_prove/pages/loading.dart';
import 'package:teori_prove/pages/splash-screen.dart';
import 'package:teori_prove/statistics/list/list_page.dart';
import 'package:teori_prove/statistics/list/statistic_list_provider.dart';
import 'package:url_launcher/url_launcher.dart';


class HomePage extends StatefulWidget {
  const HomePage(this.onSignedOut,this.loggid);
  final VoidCallback onSignedOut;
  final int loggid ;
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {

  UserModel currentUser = UserModel();

  @override
  void initState() {
    super.initState();
    home_bloc.getCategories();
//    home_bloc.getTariffType();

  }


  @override
  void didChangeDependencies() {
    final BaseAuth auth = AuthProvider.of(context).auth;
    // print("^^^^^^^^^^^^^^^^^^^^^^^^^^");
    auth.currentUserDetail().then((UserModel user) {
      // print("%---------------------------------------------******************************#");

      setState(() {
        if(user.id != null){
          currentUser = user;
        }
//        authStatus = userId == null  ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
      // print(currentUser.id);
    }).catchError((onError){
      // print("%******************************---------------------------------------------#");

      // print(onError.toString());
    });

    // print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%llll%%%%%%%%%%%%%%%%%%%%%%%%%5");

    super.didChangeDependencies();

  }
//  @override
//  void dispose() {
//    home_bloc.dispose();
//    super.dispose();
//  }

  openDetailPage(List<int> indexList,bool is_open) {
    final page = ExamPageBlocProvider(
      child: ExamPage(
          ids: indexList,
        is_open: is_open,
      ),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }

  openStatistic() {
    final page = StatisticBlocProvider(
      child: StatisticListPage(),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }

  goSignIn() {
    final page = AuthProvider(
      auth: Auth(),
      child: LoginRegisterMain(),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }

  Future<void> _signOut(BuildContext context) async {
    try {
      final BaseAuth auth = AuthProvider.of(context).auth;
      await auth.signOut();
      widget.onSignedOut();

    } catch (e) {
      // print(e);
    }
  }

  Future _mainMenuPopup() async {
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Menu'),
            children: <Widget>[
              SimpleDialogOption(

                onPressed: () {
                  Navigator.pop(context);
                  (currentUser.id == null || currentUser.paymentType == null || currentUser.paymentType == 'free')?
                    _categoriesDialog():
                    Navigator.push(
                    context,

                    MaterialPageRoute(builder: (context) => CategoriesPage()),
                  );
                },
                child:  Padding(
                  padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                  child: Text('Categories',style: TextStyle(fontSize: 16.0),),
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  openStatistic();
                },
                child: Padding(
                    padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                    child: Text('Statistics',style: TextStyle(fontSize: 16.0),)
                ),
              ),
              widget.loggid == 1 ?
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  _signOut(context);
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                  child: Text('Logout',style: TextStyle(fontSize: 16.0),),
                ),
              ):SizedBox(),
            ],
          );
        });
  }


  Future _categoryDialog(int id,bool is_open,AppTariffType appTariffType) async {
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Select to continue'),
            children: <Widget>[
              (widget.loggid == 0) ?
              SimpleDialogOption(

                onPressed: () {
                  Navigator.pop(context);
                  goSignIn();
                },
                child:  Padding(
                  padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                  child: Text('Sign In',style: TextStyle(fontSize: 16.0),),
                ),
              ):SizedBox(),

              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  appTariffType.appTariffType == 0?
                  GeneralAlerts.showStandartAlert(context, "Pricing","This servise is not avaiable"):
                  _launchURL('http://test.azweb.dk/pricing')
                  ;
                },
                child: Padding(
                    padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                    child: Text('Payment Plan',style: TextStyle(fontSize: 16.0),)
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, null);
                  openDetailPage([id],is_open);
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                  child: Text('Continue with Free',style: TextStyle(fontSize: 16.0),),
                ),
              ),
            ],
          );
        });
  }



  Future _categoriesDialog() async {
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Categories'),
            children: <Widget>[
              currentUser.id == null?
              SimpleDialogOption(

                onPressed: () {
                  Navigator.pop(context);
                  goSignIn();
                },
                child:  Padding(
                  padding: const EdgeInsets.only(top: 8,bottom: 8.0),
                  child: Text('Sign In',style: TextStyle(fontSize: 16.0),),
                ),
              ):SizedBox(height: 0,),
//              SimpleDialogOption(
//                onPressed: () {
//                  Navigator.pop(context);
//                  _launchURL('http://test.azweb.dk/pricing');
//                },
//                child: Padding(
//                    padding: const EdgeInsets.only(top: 8,bottom: 8.0),
//                    child: Text('Payment Plan',style: TextStyle(fontSize: 16.0),)
//                ),
//              ),
            ],
          );
        });
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CategoryData>(
      stream: home_bloc.subject.stream,
      builder: (context, AsyncSnapshot<CategoryData> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.error != null && snapshot.data.error.length > 0){
            return _buildErrorWidget(snapshot.data.error);
          }
          return _buildMainDataWidget(snapshot.data);

        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return LoadingPage();
        }
      },
    );
  }


  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Has no Data"),
//            Text("Error occured: $error"),
          ],
        ));
  }
  Widget _buildMainDataWidget(CategoryData categoryData) {
    return
      Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Tests",style: TextStyle(),textAlign: TextAlign.center,),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.more_vert), onPressed: ()=>  _mainMenuPopup(),)
          ],
        ),
        body: StreamBuilder(
          stream: home_bloc.subjectTarifType.stream,
          builder: (context, AsyncSnapshot<AppTariffType> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.error != null && snapshot.data.error.length > 0){
                return _buildErrorWidget(snapshot.data.error);
              }
              return _buildMainDataListWidget(categoryData,snapshot.data);

            } else if (snapshot.hasError) {
              return _buildErrorWidget(snapshot.error);
            } else {
              return LoadingPage();
            }
          },
        ),
      );
  }

  Widget _buildMainDataListWidget(CategoryData categoryData,AppTariffType appTariffType) {
//    // print("^&&&&^^^^^^^^&^^^^^^^^^^^^&&&&&&&&&&&^^^^%");
//    // print(appTariffType.appTariffType);
//    // print("^&&&&^^^^^^^^&^^^^^^^^^^^^&&&&&&&&&&&^^^^%");
    return ListView.builder(
        itemCount: categoryData.categories.length,
        itemBuilder: (context, index){

          return  index == 0 || (currentUser.paymentType != null && currentUser.paymentType != 'free' ) ? CategoryItem(is_open: true,currentUser: currentUser,appTariffType: appTariffType,openDetailPage: openDetailPage,categoryDialog: _categoryDialog, category: categoryData.categories[index],category0: categoryData.categories[0]):CategoryItem(is_open: false,currentUser: currentUser,appTariffType: appTariffType,openDetailPage: openDetailPage,categoryDialog: _categoryDialog,category: categoryData.categories[index],category0: categoryData.categories[0],) ;
        }
    );
  }

}

