import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:teori_prove/repository/repository.dart';
import 'package:teori_prove/models/question.dart';


class ExamPageBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<QuestionData> _subject =
  BehaviorSubject<QuestionData>();

  getQuestions(List<int> ids) async {
    QuestionData questionData = await _repository.fetchQuestions(ids);
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- category question-data ---");
    _subject.sink.add(questionData);
  }

  dispose() {
    _subject.close();
  }


  Future<bool> finishExam(String session_id,List<int> question_list,Map<String,int> my_answer_map) async {

    bool result = await _repository.fetchExamFinish(session_id,question_list,my_answer_map );
//    // print("!!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@@@@@####################^^^^^^^^^^^^^^^^^^^^");
//    // print(result);
    return result;
  }

  BehaviorSubject<QuestionData> get subject => _subject;

}




//class ExamPageBloc {
//  final _repository = Repository();
//  final _category_list = BehaviorSubject<List<int>>();
//  final _questions = BehaviorSubject<Future<QuestionData>>();
//
//  Function(List<int>) get fetchCategory_list => _category_list.sink.add;
//  Observable<Future<QuestionData>> get categoryQuestions => _questions.stream;
//
//  ExamPageBloc() {
//    _category_list.stream.transform(_itemTransformer()).pipe(_questions);
//  }
//
//  dispose() async {
//    _category_list.close();
////    await _questions.drain();
////    _questions.close();
//  }
//
//  _itemTransformer() {
//    return ScanStreamTransformer(
//          (Future<QuestionData> question, List<int> ids, int index) {
//        // print(index);
//        question = _repository.fetchQuestions(ids);
//        return question;
//      },
//    );
//  }
//
//  Future<bool> finishExam(String session_id,List<int> question_list,Map<String,int> my_answer_map) async {
//
//    bool result = await _repository.fetchExamFinish(session_id,question_list,my_answer_map );
//    // print("!!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@@@@@####################^^^^^^^^^^^^^^^^^^^^");
//    // print(result);
//    return result;
//  }
//
//
//
//}
