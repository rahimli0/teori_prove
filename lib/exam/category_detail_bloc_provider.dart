import 'package:flutter/material.dart';

import 'exam_bloc.dart';


class ExamPageBlocProvider extends InheritedWidget {
  final ExamPageBloc bloc;

  ExamPageBlocProvider({Key key, Widget child})
      : bloc = ExamPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ExamPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(ExamPageBlocProvider)
    as ExamPageBlocProvider)
        .bloc;
  }
}
