import 'package:flutter/material.dart';
import 'package:teori_prove/categories/categories_bloc.dart';
import 'package:teori_prove/exam/category_detail_bloc_provider.dart';
import 'package:teori_prove/exam/exam-page.dart';
import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/pages/loading.dart';
import 'package:teori_prove/pages/no-data.dart';

class CategoriesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CategoriesPageState();
  }
}

class CategoriesPageState extends State<CategoriesPage> {
  @override
  void initState() {
    super.initState();
    categories_bloc.getCategories();
  }

//  @override
//  void dispose() {
//    categories_bloc.dispose();
//    super.dispose();
//  }


  bool longPressFlag = true;
  List<int> indexList = [];

  void longPress() {
    setState(() {
      longPressFlag = true;
//      if (indexList.isEmpty) {
//        longPressFlag = false;
//      } else {
//        longPressFlag = true;
//      }
    });
  }

  openDetailPage() {
    final page = ExamPageBlocProvider(
      child: ExamPage(
          ids: indexList,
          is_open: true,
      ),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }


  @override
  Widget build(BuildContext context) {

    return StreamBuilder(
        stream: categories_bloc.subject.stream,
        builder: (context, AsyncSnapshot<CategoryData> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0){
              return ErrorData(snapshot.data.error);
            }
            return
              Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  title: Text("Categories",style: TextStyle(color:Colors.black87),),
                  backgroundColor: Colors.white,
                  actions: <Widget>[
                    indexList.length > 0 ?  MaterialButton(
                      onPressed: (){
                        openDetailPage();
                      },
                      padding: EdgeInsets.only(left: 3.0,right: 3.0),
                      child: Padding(
                        padding: EdgeInsets.only(top: 11.0,bottom: 6.0,left: 0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text('${indexList.length} ',style: TextStyle(fontSize: 13.2,color: Colors.green),),
                                Text('category selected',style: TextStyle(fontSize: 12.4),),
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 4.0)),
                            Row(
                              children: <Widget>[
                                Text('Start exam',style: TextStyle(fontSize: 13.0),),
                                Icon(Icons.arrow_forward,size: 14.5,),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ) : Text(""),
                  ],
                ),
                body:Container(
                    color: Colors.deepPurple,
                    child: buildList(snapshot.data)),);

          } else if (snapshot.hasError) {
            return ErrorData(snapshot.error);
          } else {
            return LoadingPage();
          }
        },
      );
  }

  Widget buildList(CategoryData categoryData) {
    return GridView.builder(

      gridDelegate:
      SliverGridDelegateWithFixedCrossAxisCount(

        crossAxisCount: 2,childAspectRatio: 1.6,),
      itemCount: categoryData.categories.length,

      itemBuilder: (context, index) {
        Categories category = categoryData.categories[index];
        return SelectableCategoryItem(
          // index: index,
          category: category,
          longPressEnabled: longPressFlag,
          callback: () {
            if (indexList.contains(category.id)) {
              indexList.remove(category.id);
            } else {
              indexList.add(category.id);
            }

            longPress();
          },
        );
      },
    );
  }
}



class SelectableCategoryItem extends StatefulWidget {
  // final int index;
  final Categories category;
  final bool longPressEnabled;
  final VoidCallback callback;

  const SelectableCategoryItem({Key key,this.longPressEnabled, this.callback, this.category}) : super(key: key);

  @override
  _SelectableCategoryItemState createState() => _SelectableCategoryItemState();
}

class _SelectableCategoryItemState extends State<SelectableCategoryItem> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        setState(() {
          selected = !selected;
        });
        widget.callback();
      },
      onTap: () {
        if (widget.longPressEnabled) {
          setState(() {
            selected = !selected;
          });
          widget.callback();
        }
      },
      child: Card(
        margin: const EdgeInsets.all(2.0),
        child: Container(
//          height: 50.0,
          child: Container(
//            height: 50.0,
            child: Stack(
              alignment: Alignment(-0.8, 0.8),
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Center(
                  child: Text("${widget.category.name}",style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500
                  ),
                  ),
                ),
                selected ? Icon(Icons.check,color: Colors.white,) : Text(""),
              ],
            ),
            decoration: selected
                ? BoxDecoration(color: Colors.black87, border: Border.all(color: Colors.black26,width: 0.2))
                : BoxDecoration(color: Colors.black54),
          ),
          decoration: BoxDecoration(
              color: Colors.black12,
              image: DecorationImage(
                  image: NetworkImage(widget.category.imageUrl),
                  fit: BoxFit.cover
              )
          ),
        ),
      ),
    );
  }
}