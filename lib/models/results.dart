class AllResults {
  List<ExamResults> examResults= [];
  String error = '';

//  AllResults(this.examResults);

  AllResults.fromJson(Map<String, dynamic> json) {
    // print("..........................................@@..........................................@@");

    if (json['results'] != null) {
      // print(json['results']);
      // print("@.@@..........................................@@");
//      List<ExamResults> examResults =  [];
      Map<String,dynamic> list1 = json['results'];
      for (var entry in list1.entries) {
          Map<String, dynamic> value = entry.value[0];

          examResults.add(new ExamResults.fromJson(value));

      }



//      json['results'].forEach((key, v) =>
//        v != [] ?? examResults.add(new ExamResults.fromJson(json['results']['7xzt2l85sflmn42'][0]))
//      );
      // print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
      // print(examResults.length);
//      this.examResults = examResults
      // print("@...@@..........................................@@");
//      json['results'].forEach((v) {
//        examResults.add(new ExamResults.fromJson(v));
//      });
    }
  }


//  AllResults.fromJson(Map<String, dynamic> json)
//      : examResults =
//  (json["results"] as List).map((i) => new ExamResults.fromJson(i)).toList(),
//        error = "";

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.examResults != null) {
      data['results'] = this.examResults.map((v) => v.toJson()).toList();
    }
    return data;
  }

  AllResults.withError(String errorValue)
      : examResults = List(),
        error = errorValue;

}

class ExamResults {
  String sessionId;
  String time;
  String timestamp;

  ExamResults(
      {
        this.sessionId,
        this.time,
        this.timestamp
      }
      );

  ExamResults.fromJson(Map<String, dynamic> json) {
    // print("sdsdsdsdsd------------------------sdsdsdsdsd------------------------");
    // print(json['time']);
    sessionId = json['session_id'].toString();
    time = json['time'].toString();
    timestamp = json['timestamp'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timestamp'] = this.sessionId;
    data['time'] = this.time;
    data['timestamp'] = this.timestamp;
    return data;
  }
}

class Results {
  int questionId;
  String sessionId;
  int answer;
  int correctAnswer;
  String time;
  int timestamp;

  Results(
      {this.questionId,
        this.sessionId,
        this.answer,
        this.correctAnswer,
        this.time,
        this.timestamp});

  Results.fromJson(Map<String, dynamic> json) {
    questionId = json['question_id'];
    sessionId = json['session_id'];
    answer = json['answer'];
    correctAnswer = json['correct_answer'];
    time = json['time'];
    timestamp = json['timestamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question_id'] = this.questionId;
    data['session_id'] = this.sessionId;
    data['answer'] = this.answer;
    data['correct_answer'] = this.correctAnswer;
    data['time'] = this.time;
    data['timestamp'] = this.timestamp;
    return data;
  }
}