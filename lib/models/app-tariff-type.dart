class AppTariffType {
  int appTariffType;
  String error;

  AppTariffType(this.error, {this.appTariffType});

  AppTariffType.fromJson(Map<String, dynamic> json) {
    appTariffType = json['app_tariff_type'];
    error='';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['app_tariff_type'] = this.appTariffType;
    return data;
  }
  AppTariffType.withError(String errorValue)
      : appTariffType = -1,
        error = errorValue;
}