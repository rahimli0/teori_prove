class CategoryData {
  final List<Categories> categories;
  final String error;

  CategoryData(this.categories, this.error);

  CategoryData.fromJson(Map<String, dynamic> json)
      : categories =
  (json["categories"] as List).map((i) => new Categories.fromJson(i)).toList(),
        error = "";

  CategoryData.withError(String errorValue)
      : categories = List(),
        error = errorValue;
}


class Categories {
  int id;
  String name;
  String createdAt;
  String updatedAt;
  String imageUrl;
  int questionsCount;

  Categories(
      {this.id,
      this.name,
      this.createdAt,
      this.updatedAt,
      this.imageUrl,
      this.questionsCount});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    imageUrl = json['image_url'];
    questionsCount = json['questions_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['image_url'] = this.imageUrl;
    data['questions_count'] = this.questionsCount;
    return data;
  }
}