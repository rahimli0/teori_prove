class QuestionData {
  List<Questions> questions;
  final String error;


  QuestionData(this.questions, this.error);

//  QuestionData.fromJson(Map<String, dynamic> json) {
////    // print(json['questions'].toString());
//    if (json['questions'] != null) {
//      questions = new List<Questions>();
//      json['questions'].forEach((v) {
////        // print(v.toString());
//        questions.add(new Questions.fromJson(v));
//      });
//    }
//  }

  QuestionData.fromJson(Map<String, dynamic> json)
      : questions =
  (json["questions"] as List).map((i) => new Questions.fromJson(i)).toList(),
        error = "";


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.questions != null) {
      data['questions'] = this.questions.map((v) => v.toJson()).toList();
    }
    return data;
  }

  QuestionData.withError(String errorValue)
      : questions = List(),
        error = errorValue;
}

class Questions {
  int id;
  int categoryId;
  String text;
  int answer;
  String imageUrl;
  String audioUrl;
  int userId;
  int parentId;
  String createdAt; 
  String updatedAt;
  List<Questions> subQuestions;

  Questions(
      {this.id,
      this.categoryId,
      this.text,
      this.answer,
      this.imageUrl,
      this.audioUrl,
      this.userId,
      this.parentId,
      this.createdAt,
      this.updatedAt,
      this.subQuestions});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    text = json['text'];
    answer = json['answer'];
    imageUrl = json['image_url'];
    audioUrl = json['audio_url'];
    userId = json['user_id'];
    parentId = json['parent_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
     if (json['sub_questions'] != null) {
       subQuestions = new List<Questions>();
       json['sub_questions'].forEach((v) {
         subQuestions.add(new Questions.fromJson(v));
       });
     }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['category_id'] = this.categoryId;
    data['text'] = this.text;
    data['answer'] = this.answer;
    data['image_url'] = this.imageUrl;
    data['audio_url'] = this.audioUrl;
    data['user_id'] = this.userId;
    data['parent_id'] = this.parentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subQuestions != null) {
      data['sub_questions'] = this.subQuestions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}



class ResultQuestion {
  int id;
  int categoryId;
  String text;
  int answer;
  Null imageUrl;
  String audioUrl;
  int userId;
  int parentId;
  String createdAt;
  String updatedAt;
  int userAnswer;

  ResultQuestion(
      {this.id,
        this.categoryId,
        this.text,
        this.answer,
        this.imageUrl,
        this.audioUrl,
        this.userId,
        this.parentId,
        this.createdAt,
        this.updatedAt,
        this.userAnswer});

  ResultQuestion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    text = json['text'];
    answer = json['answer'];
    imageUrl = json['image_url'];
    audioUrl = json['audio_url'];
    userId = json['user_id'];
    parentId = json['parent_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    userAnswer = json['user_answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['category_id'] = this.categoryId;
    data['text'] = this.text;
    data['answer'] = this.answer;
    data['image_url'] = this.imageUrl;
    data['audio_url'] = this.audioUrl;
    data['user_id'] = this.userId;
    data['parent_id'] = this.parentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['user_answer'] = this.userAnswer;
    return data;
  }
}