import 'package:flutter/material.dart';
import 'package:teori_prove/models/app-tariff-type.dart';
import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/models/user-repository.dart';
class CategoryItem extends StatelessWidget {
  final bool is_open;
  final Function categoryDialog;
  final Function openDetailPage;

  final UserModel currentUser;
  final Categories category;
  final AppTariffType appTariffType;
  final Categories category0;

  const CategoryItem({Key key, this.is_open, this.categoryDialog, this.category, this.openDetailPage, this.category0, this.currentUser, this.appTariffType})
      : super(key: key);

//  const CategoryItem({Key key, this.category}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {
            is_open ?
            openDetailPage([category.id],is_open)
            : categoryDialog(category0.id,is_open,appTariffType)
      },
      child: Card(
        margin: const EdgeInsets.all(1.0),
        child: Container(
//          height: 50.0,
          child: Container(
            height: 130.0,
            child: Stack(
              alignment: Alignment(0, 0.8),
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Center(
                  child: Text(
                    category.name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                !is_open || currentUser.paymentType == 'paid'
                    ? Text("")
                    : OutlineButton(
                        padding: EdgeInsets.all(1.0),
                        onPressed: () => {},
                        child: Text(
                          "Free",
                          style: TextStyle(color: Colors.white),
                        ),
                        borderSide: BorderSide(
                            width: 0.5,
                            color: Colors.white,
                            style: BorderStyle.solid),
                      ),
              ],
            ),
            decoration: BoxDecoration(color: Colors.black38),
          ),
          decoration: BoxDecoration(
              color: Colors.black12,
              image: DecorationImage(
                  image: NetworkImage(
                      category.imageUrl
                    ),
                  fit: BoxFit.cover)),
        ),
      ),
    );
  }
}
