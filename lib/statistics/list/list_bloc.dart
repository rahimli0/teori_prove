import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:teori_prove/models/results.dart';
import 'package:teori_prove/repository/repository.dart';
//
//class StatisticBloc {
//  final _repository = Repository();
//  final _userId = BehaviorSubject<int>();
//  final _statistics = BehaviorSubject<Future<AllResults>>();
//
//  Function(int) get fetchUserId => _userId.sink.add;
//  Observable<Future<AllResults>> get statistics => _statistics.stream;
//
//  StatisticBloc() {
//    _userId.stream.transform(_itemTransformer()).pipe(_statistics);
//  }
//
//  dispose() async {
//    await _userId.close();
////    await _questions.drain();
//    await _statistics.close();
//  }
//
//  _itemTransformer() {
//    return ScanStreamTransformer(
//          (Future<AllResults> statistic, int id, int index) {
//        // // print(index);
//        statistic = _repository.fetchAllStatistic(id);
//        return statistic;
//      },
//    );
//  }
//}




class StatisticBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<AllResults> _subject =
  BehaviorSubject<AllResults>();

  getResults() async {
    AllResults allResults = await _repository.fetchAllStatistic();
    // // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    // // print("--- AllResults -data ---");
    _subject.sink.add(allResults);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<AllResults> get subject => _subject;

}

final statistic_bloc = StatisticBloc();