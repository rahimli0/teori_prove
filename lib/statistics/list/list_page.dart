import 'dart:async';
import 'package:flutter/material.dart';
import 'package:teori_prove/models/question.dart';
import 'package:teori_prove/models/results.dart';
import 'package:teori_prove/models/user-repository.dart';
import 'package:teori_prove/pages/loading.dart';
import 'package:teori_prove/pages/loading2.dart';
import 'package:teori_prove/pages/no-data.dart';




import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/statistics/detail/detail_page.dart';
import 'package:teori_prove/statistics/detail/statistic_detail_provider.dart';
import 'package:teori_prove/statistics/list/list_bloc.dart';
import 'package:teori_prove/statistics/list/statistic_list_provider.dart';


class StatisticListPage extends StatefulWidget {

  const StatisticListPage();
  @override
  _StatisticListPageState createState() {
    return _StatisticListPageState(
    );
  }
}

class _StatisticListPageState extends State<StatisticListPage> {


  UserModel currentUser = UserModel();

  StatisticBloc statisticBloc;

  @override
  void initState() {
    super.initState();
  }


  @override
  void didChangeDependencies() {
    // // print("recreated");
    statisticBloc = StatisticBlocProvider.of(context);
    statisticBloc.getResults();

    // // print("lalalalalllalalalalallallalalalalalllalalalalallallalalalalalllalalalalallallalalalalalllalalalalallalla");

//    _initAudioPlayer();



    final BaseAuth auth = AuthProvider.of(context).auth;
    // // print("^^^^^^^^^^^^^^^^^^^^^^^^^^");
    auth.currentUserDetail().then((UserModel user) {
      // // print("%---------------------------------------------******************************#");

      setState(() {
        if(user.id != null){
          currentUser = user;
        }
//        authStatus = userId == null  ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
      // // print(currentUser.id);
      // // print("%---------------------------******************************------------------#");
    }).catchError((onError){
      // // print("%******************************---------------------------------------------#");

      // // print(onError.toString());
    });


    super.didChangeDependencies();
  }



  _openStatisticDetail(String sessionId) {
    final page = StatisticDetailBlocProvider(
      child: StatisticPageDetail(session_id: sessionId,),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }

//  @override
//  void dispose() {
//    bloc.dispose();
////    // // print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//    super.dispose();
////    // print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//
//  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(

          appBar: AppBar(
            actions: <Widget>[
            ],
            title: Text("Statistic"),
          ),
          body:
          StreamBuilder<AllResults>(
            stream: statisticBloc.subject.stream,
            builder: (context, AsyncSnapshot<AllResults> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.error != null && snapshot.data.error.length > 0){
                  return ErrorData(snapshot.data.error);
                }
                return _buildMainWidget(snapshot.data);

              } else if (snapshot.hasError) {
                return ErrorData(snapshot.error);
              } else {
                return LoadingPage();
              }
            },
          )
        );
  }


  Widget _buildMainWidget(AllResults data) {
    return ListView.builder(
      itemCount: data
          .examResults.length,
      itemBuilder: ((BuildContext context, int index){
        ExamResults examResultItem = data
            .examResults[index];
        return GestureDetector(
          onTap: (){
            _openStatisticDetail(examResultItem.sessionId);

          },
          child: Card(
            child: ListTile(
              key: Key("${index}"),
              leading: Icon(Icons.insert_chart),
              title: Text("Exam ${index+1} ${examResultItem.time}",textAlign: TextAlign.center,),
            ),
          ),
        );
      }),
    );}
}
