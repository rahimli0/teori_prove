import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' show Client;
import 'package:teori_prove/authentication/user-repository.dart';
import 'package:teori_prove/models/app-tariff-type.dart';
import 'package:teori_prove/models/category.dart';
import 'package:dio/dio.dart';
import 'package:teori_prove/models/question.dart';
import 'package:teori_prove/models/result-detail.dart';
import 'package:teori_prove/models/results.dart';
import 'package:device_info/device_info.dart';
import 'package:teori_prove/models/user-repository.dart';



class DataProvider {

  Future<AllResults> fetchStatisticList() async {
    Dio dio = new Dio();
    Response response;
    UserModel user = UserModel();
    UserRepository userRepository = UserRepository();
    user = await userRepository.getUser();
    int userId;
    userId = user.id;
    // print(userId);
    // print("----------------------------------------------@@");
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
    var form_data = {};
    if (userId != null) {
//      // print("--------------- user ---------------");
//      // print(userId);

      form_data = {
//        'user_id':'${userId.toString()}',
        'user_id':'${user.id.toString()}',
      };
    }else {
//      // print("--------------- device ---------------");
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String device_id = '';
      if (Platform.isAndroid) {
        // print(Platform.isAndroid);
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        device_id = androidDeviceInfo.androidId; // unique ID on Android
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
        device_id = iosDeviceInfo.identifierForVendor; // unique ID on iOS
      }
//      // print("-------------- device_id ${device_id.toString()} ---------------");
      form_data = {"device_id": "${device_id.toString()}"};
    }

    try {
      response = await dio.post("http://test.azweb.dk/api/answer/statistics",
          data: json.encode(form_data));
      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON
//        // print("bvaaaa");
        AllResults allResults = AllResults.fromJson(response.data);

        // print(allResults.examResults);
//        // print("bvaaaab");

        return AllResults.fromJson(response.data);
      } else {
//        // print("Exception occured: error stackTrace: stacktrace");
        return AllResults.withError("error");
//        AllResults allResults = AllResults([]);
//        // print("braaaa");
//        return allResults;
        // If that call was not successful, throw an error.
        throw Exception('Failed to load categories');
      }
    } on DioError catch(e,stacktrace) {
//      // print("errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror");
//      // print("Exception occured: $e stackTrace: $stacktrace");
      return AllResults.withError("$e");
//      // print("Error ${e.response.data}");
//      AllResults allResults = AllResults(examResults: []);
//      return allResults;
    }


      // print("ppppppppppppppppppdddpppppppppppppppppppppp");
      // print(response.data);
      // print("ppppppppppppppppppdddpppppppppppppppppppppp");
//    }
//    else{
//      // print("bblllfffffflllllllllbblllllllllllllllbblllllllllllllllbblllllllllgggggggg");
//      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//      String device_id = '';
//      if (Platform.isAndroid) {
//        // print(Platform.isAndroid);
//        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
//        device_id = androidDeviceInfo.androidId; // unique ID on Android
//      } else if (Platform.isIOS) {
//        IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
//        device_id = iosDeviceInfo.identifierForVendor; // unique ID on iOS
//      }
//      // print("bblllfffffflllllllllbblllllllllllllllbblllllllllllllllbblllllllllllllll");
//      var form_data = {"device_id": "${device_id.toString()}"};
//      // print(device_id);
//      // print(form_data);
//      // print(json.encode(form_data));
//
//      try {
//        response = await dio.post("http://test.azweb.dk/api/answer/statistics",
//            data: json.encode(form_data));
//        if (response.statusCode == 200) {
//          // If the call to the server was successful, parse the JSON
//          // print("bvaaaa");
//          AllResults allResults = AllResults.fromJson(response.data);
//
//          // print(allResults.examResults);
//          // print("bvaaaab");
//
//          return AllResults.fromJson(response.data);
//        } else {
//          // print("braaaa");
//          return null;
//          // If that call was not successful, throw an error.
//          throw Exception('Failed to load categories');
//        }
//      } on DioError catch(e) {
//        // print("Error zad${e.response.statusCode}");
//        return null;
//      }
//
//
//      // print("bblllllllllllllllbblllllllllllllllbblllllllllllllllbblllllllllllllll");
//    }
    // print(response.data);

    // print("data");

  }


  Future<CategoryData> fetchCategoryList() async {
    Dio dio = new Dio();
    Response response;
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
    // print("-----------------------------------------");
    try {
      response = await dio.get("http://test.azweb.dk/api/category");
      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON
        return CategoryData.fromJson(response.data);
      } else {
//        // print("Exception occured: error stackTrace: stacktrace");
        return CategoryData.withError("error");
        // If that call was not successful, throw an error.
//        throw Exception('Failed to load categories');
      }
    }on DioError catch(e, stacktrace) {
//      // print("Exception occured: $e stackTrace: $stacktrace");
      return CategoryData.withError("$e");

//      CategoryData categoryData = CategoryData(categories: [],);
//      return categoryData;
    }
  }



  Future<AppTariffType> fetchAppTariffType() async {
    Dio dio = new Dio();
    Response response;
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
    // print("-----------------------------------------");
    try {
      response = await dio.get("http://test.azweb.dk/api/app_tariff_type");
      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON
        return AppTariffType.fromJson(response.data);
      } else {
//        // print("Exception occured: error stackTrace: stacktrace");
        return AppTariffType.withError("error");
        // If that call was not successful, throw an error.
//        throw Exception('Failed to load categories');
      }
    }on DioError catch(e, stacktrace) {
//      // print("Exception occured: $e stackTrace: $stacktrace");
      return AppTariffType.withError("$e");

//      CategoryData categoryData = CategoryData(categories: [],);
//      return categoryData;
    }
  }

  Future<QuestionData> fetchQuestion(List<int> categoryIds) async {

    Dio dio = new Dio();
    Response response;
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
//    // print("*&*&&&&&&&&&*************************&&&&&&&&&&&&&&&&&");
//    // print(categoryIds.toString());
//    // print(categoryIds.join(','));

    try {
      response = await dio.get(
          "http://test.azweb.dk/api/category/questions?categories=[${categoryIds
              .join(',')}]");
      if (response.statusCode == 200) {
//        // print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
//      // print(response.data['questions'].toString());
        QuestionData questionData = QuestionData.fromJson(response.data);
        // print("------------------------------------------");
        return questionData;
      } else {
//        // print("Exception occured: error stackTrace: stacktrace");
        return QuestionData.withError("error");
//        QuestionData questionData = QuestionData(questions: []);
////      return questionData;
//        return questionData;
      }
    }on DioError catch(e,stacktrace) {
//      // print("errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror");
//      // print("Exception occured: $e stackTrace: $stacktrace");
      return QuestionData.withError("$e");
//
//      // print(e.response.data);
//      QuestionData questionData = QuestionData(questions: []);
////      return questionData;
//      return questionData;
//      throw Exception('Failed to load questions');
    }
  }


  Future<ResultQuestionData> fetchResultQuestion(String sessionId) async {

    Dio dio = new Dio();
    UserRepository userRepository = UserRepository();
    Response response;
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
//    // print("*&*&&&&&&&&&*************ddd************&&&&&&&&&&&&&&&***&&");
    // print(sessionId);
    UserModel user = UserModel();
    user = await userRepository.getUser();
//    // print(categoryIds.join(','));

//    // print("formDataformDataformDataformDataformDataformDataformDataformDataformDataformDataformData");


    var form_data = {};
    if (user.id != null) {
//      // print("--------------- user detail ---------------");
//      // print(user.id);

      form_data = {
//        'user_id':'${user.id.toString()}',
//        'session_id':sessionId,
        'session_id':sessionId.toString(),
        'user_id':'${user.id.toString()}',
      };
    }else {
//      // print("--------------- device detail ---------------");
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String device_id = '';
      if (Platform.isAndroid) {
//        // print(Platform.isAndroid);
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        device_id = androidDeviceInfo.androidId; // unique ID on Android
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
        device_id = iosDeviceInfo.identifierForVendor; // unique ID on iOS
      }
//      // print("-------------- device_id ${device_id.toString()} ---------------");
      form_data = {
        "device_id": "${device_id.toString()}",
        'session_id':sessionId,
      };
    }


    try{
      response = await dio.post("http://test.azweb.dk/api/answer/statistics/session",data: json.encode(form_data));
      if (response.statusCode == 200) {
//        // print("&&&&&&&&&&&&&&&&&&&& detail  &&&&&&&&&&&&&&&&&&&");
//      // print(response.data['questions'].toString());
        ResultQuestionData questionData = ResultQuestionData.fromJson(response.data);
//        // print(response.data);
//        // print(questionData.questions);
//        // print("------------------------------------------");
        return questionData;
      } else {
//        // print("5000000000000000000000000000000");
        ResultQuestionData questionData = ResultQuestionData(questions: []);
        return questionData;
//      // print("error");
//      throw Exception('Failed to load questions');
      }
    } on DioError catch(e) {

//      // print("errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror");

//      // print(e.response.data);
//      // print(e.response.request.data);
      ResultQuestionData questionData = ResultQuestionData(questions: []);
//      return questionData;
      return questionData;
      throw Exception('Failed to load questions');
    }



  }


  Future<bool> fetchExamFinish(String sessionId,List<int> question_list,Map<String,int> my_answer_map) async {

    Dio dio = new Dio();
//    // print("*&*&&&&&&&&&*************^^^^^^*-----********&&&&&&&&&&&&&&&***&&");
    UserRepository userRepository = UserRepository();
    Response response;
    // var res = await http.get("http://test.azweb.dk/api/category/questions?categories=[1]" );
    // if(res)
//    // print("*&*&&&&&&&&&*************^^^^^^*cc********&&&&&&&&&&&&&&&***&&");
//    // print(categoryIds.toString());
    UserModel user = UserModel();
    user = await userRepository.getUser();
    var formData = {};

    if (user.id != null) {
//      // print("--------------- user detail ---------------");
//      // print(user.id);

      formData = {
        'user_id':user.id.toString(),
        'session_id':sessionId.toString(),
        'question_list':json.encode(question_list),
        'answers':json.encode(my_answer_map),
      };
    }else {
//      // print("--------------- device detail ---------------");
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String device_id = '';
      if (Platform.isAndroid) {
        // print(Platform.isAndroid);
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        device_id = androidDeviceInfo.androidId; // unique ID on Android
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
        device_id = iosDeviceInfo.identifierForVendor; // unique ID on iOS
      }
//      // print("-------------- device_id ${device_id.toString()} ---------------");
      formData = {
        "device_id": "${device_id.toString()}",
        'session_id':sessionId.toString(),
        'question_list':json.encode(question_list),
        'answers':json.encode(my_answer_map),
      };
    }
//    var request_data = {
//    };
//    // print("66666666666666666666666666666");
//    // print(user.id);
//    // print(sessionId);
//    // print(question_list);
//    // print(my_answer_map);
    try{
      response = await dio.post("http://test.azweb.dk/api/answer",data: json.encode(formData));
      if (response.statusCode == 200) {
//        // print("yeeeeeeeeeeeaaaaaaaaaaaaaaahhhhhhhhhhhhh");
//        // print(response.data.toString());
//        // print(response.request.data);
//        // print("yeeeeeeeeeeeaaaaaaaaaaaaaaahhhhhhhhhhhhh");
//      // print(response.data['questions'].toString());
//        ResultQuestionData questionData = ResultQuestionData.fromJson(response.data);
//        // print("------------------------------------------");
        return true;
      } else {
//        // print("5000000000000000000000000000000");
        return false;
//      // print("error");
//      throw Exception('Failed to load questions');
      }
    } on DioError catch(e) {
//        // print("Error ${e.response.data}");
        return false;
    }

//    return false;
  }



}


