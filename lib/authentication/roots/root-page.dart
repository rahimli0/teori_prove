
import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/pages/login_page.dart';


import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/home/home_page.dart';
//import 'package:teori_prove/authentication/test-home.dart';
import 'package:teori_prove/pages/splash-screen.dart';

class MainRootPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainRootPageState();
}

enum AuthStatus {
  notDetermined,
  notSignedIn,
  signedIn,
}

class _MainRootPageState extends State<MainRootPage> {
  AuthStatus authStatus = AuthStatus.notDetermined;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    final BaseAuth auth = AuthProvider.of(context).auth;
    // print("^^^^^^^^^^^^^^^^^^^^^^^^^^");
    auth.currentUser().then((int userId) {
      // print("%####################################");
      // print(userId);
      // print(authStatus);
      // print("_*_*_*_*__*_*_*_*_*_*_*_*_*_*_*_*_*_*_*__*_*_*_*_*_*_*_*_*_*_*");
      setState(() {
        authStatus = userId == null  ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
    }).catchError((onError){
      // print(onError.toString());
    });
  }

  void _signedIn() {
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  void _signedOut() {
    setState(() {
      authStatus = AuthStatus.notSignedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("______________________________");
    // print(authStatus);
    switch (authStatus) {
      case AuthStatus.notDetermined:
        return _buildWaitingScreen();
      case AuthStatus.notSignedIn:
        return HomePage(
          _signedOut,
          0
        );
        return LoginPage(
          onSignedIn: _signedIn,
        );
      case AuthStatus.signedIn:
        return HomePage(
          _signedOut,
          1,
        );
    }
    return null;
  }

  Widget _buildWaitingScreen() {
    return SplashScreen();
  }
}